protoc --proto_path=$PWD/tensorflow                                         --go_out=plugins=grpc:$PWD/tensorflow $PWD/tensorflow/*.proto
protoc --proto_path=$PWD/db_service                                         --go_out=plugins=grpc:$PWD/db_service $PWD/db_service/*.proto
protoc --proto_path=$PWD/tensorflow_serving -I$GOPATH/src -I$PWD/tensorflow --go_out=plugins=grpc:$PWD/tensorflow_serving $PWD/tensorflow_serving/*.proto
protoc --proto_path=$PWD/opencv_service     -I$GOPATH/src                  --go_out=plugins=grpc:$PWD/opencv_service $PWD/opencv_service/*.proto
