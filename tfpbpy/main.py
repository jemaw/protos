from tfpbpy import model_pb2
from tfpbpy import prediction_service_pb2_grpc
from tfpbpy import predict_pb2
from tfpbpy import tensor_shape_pb2
from tfpbpy import tensor_pb2
from tfpbpy import types_pb2

from grpc.beta import implementations
import gevent
import grpc
from gevent import select

from time import time

import cv2
import numpy as np
import sys


overlap = 0
def split_image_in_parts(img, config):
    part_size = int(np.mean( [float(config['crop_min']), float(config['crop_max']) ]))
    img_size = img.shape

    num_parts_y = int(np.ceil(img_size[0]/(part_size*(1.0-overlap))))
    num_parts_x = int(np.ceil(img_size[1]/(part_size*(1.0-overlap))))

    embedded_img = np.ones((int(num_parts_y*part_size), int(num_parts_x*part_size), int(config['image_shape'][2])), dtype=np.uint8)*0 # padding gray
    embedded_img[(embedded_img.shape[0]-img_size[0])/2:-(embedded_img.shape[0]-img_size[0])/2, (embedded_img.shape[1]-img_size[1])/2:-(embedded_img.shape[1]-img_size[1])/2, : ] = img

    parts = np.ones((num_parts_y, num_parts_x, part_size, part_size, int(config['image_shape'][2])), dtype=np.uint8)*0
    parts_coords = np.zeros((num_parts_y, num_parts_x, 4), dtype=np.int32)

    y_start_coord = int(np.ceil((embedded_img.shape[0]-img_size[0])/2.0-part_size*overlap))
    x_start_coord = int(np.ceil((embedded_img.shape[1]-img_size[1])/2.0-part_size*overlap))

    y_end_coord = int(np.ceil(img_size[0]+y_start_coord+np.ceil(part_size*overlap)))
    x_end_coord = int(np.ceil(img_size[1]+x_start_coord+np.ceil(part_size*overlap)))

    y_steps = int( np.ceil(( y_end_coord - y_start_coord ) / float(num_parts_y )))
    x_steps = int( np.ceil(( x_end_coord - x_start_coord ) / float(num_parts_x )))

    for y in range(num_parts_y):
        for x in range(num_parts_x):
            y_start = int(np.ceil(y*y_steps+y_start_coord))
            x_start = int(np.ceil(x*x_steps+x_start_coord))

            part_coords = np.array([y_start, y_start+part_size, x_start, x_start+part_size])
            part = embedded_img[part_coords[0]:part_coords[1], part_coords[2]:part_coords[3], : ]
            parts[y,x,:,:,:] = part
            parts_coords[y,x,:] = part_coords

    return parts, part_size, num_parts_y, num_parts_x, parts_coords, y_steps, x_steps

def make_request_proto(img_lst, modelname):
    shape = tensor_shape_pb2.TensorShapeProto()
    d = shape.dim.add()
    d.name = "image/encoded"
    d.size = len(img_lst)

    tensor = tensor_pb2.TensorProto(
        string_val = img_lst,
        tensor_shape = shape,
        dtype = types_pb2.DT_STRING)

    spec = model_pb2.ModelSpec(name = modelname)
    return predict_pb2.PredictRequest(
            model_spec = spec,
            inputs = {"images" : tensor}
            )

def sendreq(imgs, stub, i):
    strlst = []
    for img in imgs:
        img = cv2.resize(img, (128,128))
        img_string = cv2.imencode(".jpg", img)[1].tostring()
        strlst.append(img_string)


    req = make_request_proto(strlst, "default")
    t = time()
    result = stub.Predict(req, 10.0)
    predicttime = time() - t
    return result, predicttime

if __name__ == '__main__':

    with open("/home/jean/pictures/testimages/2000.jpg", 'rb') as f:
        img = f.read()

    server = "localhost:8500"
    # host, port = server.split(":")
    # channel = grpc.insecure_channel(host, int(port))
    channel = grpc.insecure_channel(server)
    stub = prediction_service_pb2_grpc.PredictionServiceStub(channel)

    test = []

    config = {}
    config['crop_min'] = 256 
    config['crop_max'] = 320

    t = time()
    # img = cv2.imread("/home/jean/pictures/testimages/2000.jpg",0)
    t_split = time()

    img = np.fromstring(img, np.uint8)
    img = cv2.imdecode(img, 0 )
    config['image_shape']  = [128,128,1]
    img = img.reshape((img.shape[0],img.shape[1],1))


    # x = split_image_in_parts(img, config)
    parts, part_size, num_parts_y, num_parts_x, parts_coords, y_steps, x_steps = split_image_in_parts(img, config)

    print("time to split: ", time() - t_split)
    parts = parts.reshape(parts.shape[0]*parts.shape[1],parts.shape[2],parts.shape[3])

    i = 1
    wanted_batch_size = 31
    while ( i < len(parts)):
    # for i,part in enumerate(parts):
        if ( i + wanted_batch_size < len(parts)):
            test.append(gevent.spawn(sendreq,parts[i:i+wanted_batch_size:,:],stub,i))
            # resp = sendreq(parts[i:i+wanted_batch_size,:,:], stub, i)
            i += wanted_batch_size
        else:
            # resp = sendreq(parts[i:,:,:],stub,i)
            test.append(gevent.spawn(sendreq,parts[i:,:,:],stub,i))
            i += len(parts) - 1 -i
            break
        print(i)




        # if i > 10:
        #     break
        # test.append(gevent.spawn(sendreq,part,stub,i))
        # resp = sendreq(img,stub,i)
        # print ("1:",resp[1])
        # print(resp[0].outputs["classes"].string_val)
    gevent.joinall(test)



    print("time for complete shit img: ", time() - t)
    # t = 0
    # for res in test:
    #     print (res.value[1])
    #     t += res.value[1]

    # print("predicttime: ", t)

    # print(test[-1].value)

    print ("test size: ", len(test))
    count = 0
    for t in test:
        for img in t.value[0].outputs["classes"].string_val:
            print (len(img))
            testimg = np.fromstring(img, np.uint8)
            testimg = cv2.imdecode(testimg,1)
            cv2.imshow("test",testimg)
            cv2.waitKey(0)
            count+=1
    print ("count: ",count)
    # testresp = test[-1].value.outputs["classes"].string_val[0]
    # testimg = np.fromstring(testresp, np.uint8)
    # testimg = cv2.imdecode(testimg,1)
    # cv2.imshow("test",testimg)
    # cv2.waitKey(0)




    # gevent.joinall([
    #     gevent.spawn(sendreq(img,stub)),
    #     gevent.spawn(sendreq(img,stub)),
    #     gevent.spawn(sendreq(img,stub)),
    #     ])
